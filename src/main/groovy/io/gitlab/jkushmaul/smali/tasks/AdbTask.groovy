package io.gitlab.jkushmaul.smali.tasks


//package io.gitlab.jkushmaul.gradle.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.gradle.process.ExecResult

class AdbCommand {
    static final String adb_file = "adb"
    static String getAdbFile() {
        return adb_file
    }
    List<String> argv

    ExecResult exec_result

    ByteArrayOutputStream stdout
    ByteArrayOutputStream stderr

    Project project
    String android_device
    AdbCommand(Project project, String android_device) {
        this.project = project
        this.android_device = android_device
    }

    def adb(String command, String arg) {
        adb(null, command, [arg])
    }
    def adb(String command) {
        adb(null, command, null)
    }
    def adb(String command, ArrayList args) {
        adb(null, command, args)
    }
    def adb_ignore_exit_code(ArrayList pre_command, String command, ArrayList args) {
        adb_command(true, pre_command, command, args)
    }
    def adb(ArrayList pre_command, String command, ArrayList args) {
        adb_command(false, pre_command, command, args)
    }
    def adb_command(fail_silently, ArrayList pre_command, String command, ArrayList args) {
        stdout = new ByteArrayOutputStream()
        stderr = new ByteArrayOutputStream()
        argv = []
        if (pre_command != null) {
            argv += pre_command
        }
        argv += [ adb_file, "-s", android_device, command ]
        if (args != null) {
            argv += args
        }

        exec_result = project.exec {
            workingDir project.buildDir
            commandLine argv
            standardOutput = stdout
            errorOutput = stderr
            ignoreExitValue = fail_silently
        }
    }
}
abstract class AdbTask extends DefaultTask {
    def createAdbCommand(String android_device) {
        return new AdbCommand(project, android_device)
    }

}

class AdbPushTask extends AdbTask {
    @Input
    boolean uninstall = false
    @Input
    String package_name
    @Input
    String android_device
    @InputFile
    File signed_apk
    @OutputFile
    pushed_file

    @TaskAction
    def push() {
        pushed_file.delete()
        AdbCommand cmd = createAdbCommand(android_device)
        try {
            if (uninstall) {
                //toss user data
                cmd.adb_ignore_exit_code([ "timeout", "3" ], "uninstall",  [ package_name ])
            } else {
                //keep data
                cmd.adb_ignore_exit_code([ "timeout", "3" ], "uninstall", [ "-k", package_name ])
            }
            cmd.adb("install", [ "-r", signed_apk.absolutePath ])
            pushed_file.createNewFile()
        } catch (Exception ex) {
            pushed_file.delete()
            println "Push failed:"
            String sout = cmd.stdout.toString()
            if (!uninstall && sout.contains("INSTALL_FAILED_ALREADY_EXISTS")) {
                throw new GradleException("${package_name} is already installed - you must provide `uninstall = true` in task or smali ext.")
            } else {
                println sout
                throw ex
            }
        }
    }
}

class AdbDebugTask extends AdbTask {
    @Input
    String package_name
    @Input
    String android_device
    @Input
    String main_activity

    @TaskAction
    def debug() {
        AdbCommand cmd = createAdbCommand(android_device)

        try {
            println "ADB: Stopping ${package_name} on ${android_device}"
            cmd.adb("shell", [ "am", "force-stop", package_name ])

            println "ADB: Starting ${package_name}/.${main_activity} on ${android_device}, waiting for debugger"
            cmd.adb null, "shell", ["am", "start", "-D", "-n", "${package_name}/.${main_activity}" ] as ArrayList

            println "ADB: Fetching remote debugging jdwp"
            System.sleep(1000)
            cmd.adb_ignore_exit_code(["timeout", "1" ] as ArrayList, "jdwp", null)

            def jdwp = cmd.stdout.toString()
            if (jdwp == null || jdwp.isEmpty()) {
                throw new GradleException("Could not find jdwp of package")
            }

            println "ADB: Enabling remote debugging"
            cmd.adb null, "forward", ["tcp:5005", "jdwp:${jdwp}" ] as ArrayList
        } catch (Exception ex) {
            println "Debug failed: "
            println "${cmd.stdout}"
            throw ex
        }

    }
}