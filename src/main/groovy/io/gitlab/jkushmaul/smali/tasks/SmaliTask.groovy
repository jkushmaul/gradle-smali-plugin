package io.gitlab.jkushmaul.smali.tasks

import org.gradle.api.DefaultTask

//package io.gitlab.jkushmaul.gradle.tasks

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.artifacts.ResolvedArtifact
import org.gradle.api.file.Directory
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

abstract class SmaliTask extends DefaultTask {
    def JARSIGNER_EXECUTABLE="jarsigner"

    File getApkToolPath(Project project) {
        ResolvedArtifact artifact = project.configurations.url2maven.resolvedConfiguration.resolvedArtifacts.find {
            it.name == "apktool"
        }
        if (artifact == null) {
            throw new GradleException("apktool dependency was not found: are we in afterEvaluate?")
        }
        artifact.file
    }
}

class DecompileApkToSmaliTask extends SmaliTask {
    @InputFile
    File original_apk
    @OutputDirectory
    File smali_src_dir

    @TaskAction
    def decompile() {
        File apktool_jar = getApkToolPath(project)

        File done_marker = project.file("${smali_src_dir}/.decompiled")
        if (done_marker.exists()) {
            throw new GradleException("${done_marker} already exists, aborting decompilation")
        }

        def stdout = new ByteArrayOutputStream()
        try {
            println "Decompiling original_apk: ${original_apk} into  smali_src_dir: ${smali_src_dir}"
            smali_src_dir.mkdirs()
            project.exec {
                workingDir smali_src_dir
                commandLine "java", "-jar", "${apktool_jar}",
                        "d", "${original_apk}",
                        "-f", "-o", "./"
                standardOutput = stdout
                errorOutput = stdout
            }
        } catch (ex) {
            println "Decompile failed:"
            println "${stdout}"
            throw ex
        }
    }
}

class CleanSmaliSourceTask extends SmaliTask {
    @InputDirectory
    File smali_src_dir
    @TaskAction
    def clean_smali_src() {
        println "Cleaning smali_src_dir: ${smali_src_dir}"
        smali_src_dir.deleteDir()
    }
}


class CleanTask extends SmaliTask {
    @TaskAction
    def clean() {
        println "Cleaning build directory: ${project.buildDir}"
        project.buildDir.deleteDir()
    }
}

class BuildSmaliTask extends SmaliTask {
    @InputDirectory
    File smali_src_dir
    @OutputFile
    File unsigned_apk

    @TaskAction
    def build() {
        File apktool_jar = getApkToolPath(project)
        def stdout = new ByteArrayOutputStream()
        try {
            println "Building smali_src_dir: ${smali_src_dir} into unsigned_apk: ${unsigned_apk}"
            project.exec {
                commandLine "java", "-jar", "${apktool_jar}", "b", "-d", "${smali_src_dir}", "-o", "${unsigned_apk}"
                standardOutput = stdout
                errorOutput = stdout
            }
        } catch (ex) {
            println "Build failed:"
            println "${stdout}"
            throw ex
        }
    }
}

class SignApkTask extends SmaliTask {
    @InputFile
    File keystore
    @Input
    String keypass
    @Input
    String storepass
    @Input
    String alias
    @InputFile
    File unsigned_apk
    @OutputFile
    File signed_apk

    @TaskAction
    def sign() {
        if (unsigned_apk == signed_apk) {
            throw new GradleException("unsigned_apk should not equal signed_apk, to utilize build cache")
        }

        try {
            //oh the horror!
            if (JARSIGNER_EXECUTABLE == null) {
                //mocking the executable ant uses - not sure what to set it to for default
                ant.signjar(
                        jar: unsigned_apk,
                        keystore: keystore,
                        alias: alias,
                        keypass: keypass,
                        storepass: storepass,
                        signedJar: signed_apk)
            } else {
            ant.signjar(
                    jar: unsigned_apk,
                    keystore: keystore,
                    alias: alias,
                    keypass: keypass,
                    storepass: storepass,
                    signedJar: signed_apk,
                    executable: JARSIGNER_EXECUTABLE)
            }
        } catch (ex) {
            signed_apk.delete()
            throw ex
        }

    }
}