package io.gitlab.jkushmaul.smali

import org.gradle.api.Project
import org.gradle.api.file.Directory
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.internal.impldep.org.eclipse.jgit.annotations.NonNull
import org.gradle.internal.reflect.Instantiator

class SmaliExtension {
    SigningExtension signing = new SigningExtension()
    ApktoolExtension apktool

    File smali_src_dir
    File original_apk
    String android_device
    String package_name
    String main_activity

    Project project
    SmaliExtension(@NonNull Instantiator instantiator,
                   Project project) {
        this.project = project
        this.apktool = new ApktoolExtension(project)
    }
    SigningExtension signing(Closure closure) {
        signing = new SigningExtension()
        project.configure(signing, closure)
    }
}
class ApktoolExtension {
    Project project
    ApktoolExtension(Project project) {
        this.project = project
    }
    def add(String version, String sha1) {
        project.dependencies {
            url2maven("iBotPeaches:apktool:${version}") {
                ext.url = "https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_${version}.jar"
                ext.sha1 = sha1
            }
        }
    }
}
class SigningExtension {
    File keystore
    String keypass
    String storepass
    String alias
}
