# gradle-smali-plugin

This plugin fetches apktool.jar using url2maven dependnecy caching from the official URL:  
e.g.) https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_2.4.0.jar

## Configuration
You must provide a smali extension to your project:
```$groovy
smali {
    //Output for decompile, input for build: Generally ./src/smali
    File smali_src_dir
    //When decompiling, the apk to be decompiled
    File original_apk
    //The device serial from adb devices
    String android_device
    //the package name of the apk
    String package_name
    //The main activity "./MainActivity"
    String main_activity

    signing {
        //The keystore file
        File keystore
        String keypass
        String storepass
        String alias       
    }
}
```
You can load these values from a local.properties, or specify them directly

## Future ApkTool versions
You can also, outside of the extension, add a custom apktool version by using an extension function
 to add the dependency - sorry, I found no other way to achieve this:  
 ```
 smali.apktool.add("2.4.0", "5e7674f45038b03ff0e21158e72d44497fdc395d")
 ```
The URL will be manufactured by url2maven, which then downloads and installs to maven local cache.
If not provided, 2.4.0 and above hash is used in absence.
There's no guaruntee future versions will be compatable but the option is there.

## Tasks
Several tasks can then be used for working with smali code, with a non-android-project interface
to adb.
* decompile  
    Will output to smali.smali_src_dir
* build  
    Will use smali.smali_src_dir and build ${smali.package_name}-unsigned.apk
* sign  
    Will sign ${smali.package_name}-unsigned.apk to ${smali.package_name}-signed.apk
* push
    Will (optionally `uninstall`, then) install ${smali.package_name}-signed.apk
* debug
    Will launch ${smali.package_name} and wait for remote debugging to attach.

## Todo
Some of this could have likely been handled by just using existing android tasks but I found nothing of the sort for smali.

Downstream project is beginning to have some visible utilities which would benefit other projects.  Those could be shifted into this tool.